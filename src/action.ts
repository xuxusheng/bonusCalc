import { Action } from 'redux';

export enum ActTypes {
    Clear = 'Clear',
    AddUser = 'AddUser',
    DelUser = 'DelUser',
    ToggleIsFixed = 'ToggleIsFixed',
    SetValue = 'SetValue',
    SetCoefficient = 'SetCoefficient',
    SetTotal = 'SetTotal'
}

export class ActionClear implements Action {
    readonly type = ActTypes.Clear
}

export class ActionAddUser implements Action {
    readonly type = ActTypes.AddUser;

    constructor(public payload: string[]) {
    }
}

export class ActionDelUser implements Action {
    readonly type = ActTypes.DelUser;

    constructor(public payload: number[]) {
    }
}

export class ActionToggleIsFixed implements Action {
    readonly type = ActTypes.ToggleIsFixed;

    constructor(public payload: { index: number, isFixed: boolean }) {
    }
}

export class ActionSetValue implements Action {
    readonly type = ActTypes.SetValue;

    constructor(public payload: { index: number, value: number }) {
    }
}

export class ActionSetCoefficient implements Action {
    readonly type = ActTypes.SetCoefficient;

    constructor(public payload: { index: number, coefficient: number }) {
    }
}

export class ActionSetTotal implements Action {
    readonly type = ActTypes.SetTotal;

    constructor(public payload: number) {
    }
}

export type Actions =
    ActionClear
    | ActionAddUser
    | ActionDelUser
    | ActionToggleIsFixed
    | ActionSetValue
    | ActionSetCoefficient
    | ActionSetTotal;
