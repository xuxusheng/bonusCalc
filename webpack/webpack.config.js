const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')


module.exports = function makeWebpackConfig() {

    const config = {}

    config.resolve = {
        modules: ['./node_modules'],
        extensions: ['.js', '.ts', '.tsx', '.json']
    }

    config.entry = {
        app: path.resolve(__dirname, '../index.tsx')
    }

    config.output = {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].[hash].js',
    }

    config.module = {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: [/node_modules/]
        }, {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: [{ loader: 'style-loader', options: { sourceMap: true } }],
                use: [
                    { loader: 'css-loader', options: { modules: true, sourceMap: true, context: '/' } },
                    { loader: 'postcss-loader' },
                    { loader: 'sass-loader' }
                ]
            })
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: [
                    { loader: 'style-loader', options: { sourceMap: true } }
                ],
                use: [
                    { loader: 'css-loader', },
                    { loader: 'postcss-loader' }
                ]
            }),
            exclude: [/node_modules/]
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: [
                    { loader: 'style-loader', options: { sourceMap: true } }
                ],
                use: [
                    { loader: 'css-loader', }
                ]
            }),
            include: [/node_modules/]
        }, {
            test: /\.(ico|woff|woff2|ttf|eot)(\?.+)?$/,
            use: [
                { loader: 'file-loader' }
            ]
        }, {
            test: /\.(png|jpg|jpeg|gif|svg)(\?.+)?$/,
            use: [
                { loader: 'file-loader' }
            ]
        }, {
            test: /\.yaml/,
            use: [
                { loader: 'json-loader' },
                { loader: 'yaml-loader' }
            ]
        }]
    }

    config.plugins = []

    config.plugins.push(
        new ExtractTextPlugin({
            filename: '[name].[hash].css',
            disable: false
        }),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, '../src/index.html'),
            inject: 'body'
        })
    )

    config.devServer = {
        disableHostCheck: true,
        host: '0.0.0.0',
        historyApiFallback: {
            index: ''
        },
        port: 8080
    }

    return config
}()